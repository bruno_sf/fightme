package me.arkenum.fightme;

import me.arkenum.fightme.arena.ArenaManager;
import me.arkenum.fightme.config.FightMeConfigurationManager;
import me.arkenum.fightme.message.FightMeMessageManager;
import me.ddevil.shiroi.PluginSettings;
import me.ddevil.shiroi.PublicPlugin;
import me.ddevil.shiroi.command.ArrayCommandManager;
import me.ddevil.shiroi.misc.PluginColorDesign;

@PluginSettings(loadConfig = true)
public class FightMe extends PublicPlugin<FightMeMessageManager, FightMeConfigurationManager, ArrayCommandManager<FightMe>> {
    private ArenaManager arenaManager;

    @Override
    protected void doReload() {
    }

    @Override
    protected void doSetup() {
    }

    @Override
    protected void beforeInitializingManagers0() {
        arenaManager = new ArenaManager(this);
        arenaManager.setup();
    }

    @Override
    protected ArrayCommandManager<FightMe> loadCommandManager() {
        return new ArrayCommandManager<>(this);
    }

    @Override
    protected FightMeMessageManager loadMessageManager0(FightMeConfigurationManager fightMeConfigurationManager, PluginColorDesign pluginColorDesign) {
        return new FightMeMessageManager(this, pluginColorDesign, fightMeConfigurationManager);
    }

    @Override
    protected FightMeConfigurationManager loadConfigManager() {
        return new FightMeConfigurationManager(this);
    }


}
