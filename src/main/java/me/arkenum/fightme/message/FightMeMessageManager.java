package me.arkenum.fightme.message;

import me.arkenum.fightme.FightMe;
import me.arkenum.fightme.config.FightMeConfigurationManager;
import me.arkenum.fightme.config.FightMeConfigurationValue;
import me.ddevil.shiroi.message.internal.BaseMessageManager;
import me.ddevil.shiroi.misc.PluginColorDesign;

public class FightMeMessageManager extends BaseMessageManager<FightMe> {


    public FightMeMessageManager(FightMe fightMe, PluginColorDesign pluginColorDesign, FightMeConfigurationManager configManager) {
        super(
                fightMe,
                pluginColorDesign,
                configManager.getValue(FightMeConfigurationValue.PLUGIN_PREFIX, configManager.getFileConfiguration(FightMeConfigurationManager.MESSAGES_CONFIG)),
                configManager.getValue(FightMeConfigurationValue.MESSAGE_SEPARATOR, configManager.getFileConfiguration(FightMeConfigurationManager.MESSAGES_CONFIG)),
                configManager.getValue(FightMeConfigurationValue.HEADER, configManager.getFileConfiguration(FightMeConfigurationManager.MESSAGES_CONFIG)),
                configManager.getValue(FightMeConfigurationValue.COLOR_CHAR, configManager.getFileConfiguration(FightMeConfigurationManager.MESSAGES_CONFIG)).charAt(0)
        );
    }
}
