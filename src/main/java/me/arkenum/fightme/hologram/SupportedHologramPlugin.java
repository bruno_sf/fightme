package me.arkenum.fightme.hologram;

/**
 * Created by bruno on 09/11/2016.
 */
public enum SupportedHologramPlugin {
    NONE,
    HOLOGRAPHIC_DISPLAYS;
}
