package me.arkenum.fightme.hologram;

import org.bukkit.Location;

/**
 * Created by bruno on 09/11/2016.
 */
public interface CompatibleHologram {
    Location getLocation();

    void spawn(Location l);
}
