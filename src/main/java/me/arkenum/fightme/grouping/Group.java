package me.arkenum.fightme.grouping;

import java.util.List;
import java.util.UUID;

public interface Group {

    List<UUID> getPlayers();

}
