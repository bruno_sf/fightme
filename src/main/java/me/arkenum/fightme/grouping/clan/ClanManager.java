package me.arkenum.fightme.grouping.clan;

import me.ddevil.shiroi.misc.Toggleable;

/**
 * Created by bruno on 09/11/2016.
 */
public interface ClanManager extends Toggleable {
    Clan getClan(String name);
}
