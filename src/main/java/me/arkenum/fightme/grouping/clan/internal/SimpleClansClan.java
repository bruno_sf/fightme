package me.arkenum.fightme.grouping.clan.internal;

import me.arkenum.fightme.grouping.clan.Clan;
import me.arkenum.fightme.grouping.internal.BasicSquad;
import net.sacredlabyrinth.phaed.simpleclans.ClanPlayer;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class SimpleClansClan extends BasicSquad implements Clan {
    private final net.sacredlabyrinth.phaed.simpleclans.Clan clan;

    public SimpleClansClan(net.sacredlabyrinth.phaed.simpleclans.Clan clan) {
        super(clan.getMembers().stream().map(ClanPlayer::getUniqueId).collect(Collectors.toList()));
        this.clan = clan;
    }

    @Override
    public List<UUID> getLeaders() {
        return clan.getLeaders().stream().map(ClanPlayer::getUniqueId).collect(Collectors.toList());
    }

    @Override
    public String getName() {
        return clan.getName();
    }

    @Override
    public String getTag() {
        return clan.getTag();
    }
}
