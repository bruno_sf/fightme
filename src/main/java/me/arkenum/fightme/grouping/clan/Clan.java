package me.arkenum.fightme.grouping.clan;

import me.arkenum.fightme.grouping.Group;

import java.util.List;
import java.util.UUID;

public interface Clan extends Group {

    List<UUID> getLeaders();

    String getName();

    String getTag();
}
