package me.arkenum.fightme.grouping;

import java.util.UUID;

/**
 * Created by bruno on 09/11/2016.
 */
public interface Squad extends Group {

    void addMember(UUID member);

    void removeMember(UUID member);
}
