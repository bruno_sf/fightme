package me.arkenum.fightme.grouping.internal;

import me.arkenum.fightme.grouping.Squad;

import java.util.List;
import java.util.UUID;


public abstract class BasicSquad implements Squad {
    private final List<UUID> members;

    public BasicSquad(List<UUID> members) {
        this.members = members;
    }

    @Override
    public List<UUID> getPlayers() {
        return members;
    }

    @Override
    public void addMember(UUID member) {
        if (!members.contains(member)) {
            this.members.add(member);
        }
    }

    @Override
    public void removeMember(UUID member) {
        if (members.contains(member)) {
            this.members.remove(member);
        }
    }
}
