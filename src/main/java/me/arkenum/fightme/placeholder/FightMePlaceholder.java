package me.arkenum.fightme.placeholder;

import me.arkenum.fightme.arena.Arena;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

/**
 * Created by bruno on 07/11/2016.
 */
public enum FightMePlaceholder {
    NAME("name") {
        @Override
        public String response(PlaceholderRequest request) {
            Arena arena = request.getArena();
            return arena == null ? ERROR : arena.getName();
        }
    },
    ALIAS("alias") {
        @Override
        public String response(PlaceholderRequest request) {
            return "Oin eu sou um placeholder!";
        }
    },
    PLAYER_HEALTH("playerhealth") {
        @Override
        public String response(PlaceholderRequest request) {
            Player player = request.getPlayer();
            return player == null ? ERROR : String.valueOf(player.getHealth());
        }
    };

    private static final String ERROR = ChatColor.RED + "Error!";
    private final String replacer;

    FightMePlaceholder(String replacer) {
        this.replacer = replacer;
    }

    public String getReplacer() {
        return replacer;
    }

    public abstract String response(PlaceholderRequest request);
}
