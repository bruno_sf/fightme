package me.arkenum.fightme.placeholder;

import me.arkenum.fightme.arena.Arena;
import org.bukkit.entity.Player;

/**
 * Created by bruno on 07/11/2016.
 */
public class PlaceholderRequest {
    private final Arena arena;
    private final Player player;

    public PlaceholderRequest(Arena arena, Player player) {
        this.arena = arena;
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }

    public Arena getArena() {
        return arena;
    }
}
