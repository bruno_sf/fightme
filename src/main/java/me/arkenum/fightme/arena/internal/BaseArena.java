package me.arkenum.fightme.arena.internal;

import me.arkenum.fightme.arena.Arena;
import me.ddevil.shiroi.misc.internal.BaseNameable;
import org.bukkit.Location;
import org.bukkit.World;

import javax.annotation.Nonnull;

/**
 * Created by bruno on 07/11/2016.
 */
public class BaseArena extends BaseNameable implements Arena {
    private final Location attackerSpawn;
    private final Location defenderSpawn;
    private final Location spectatorSpawn;
    private final Location exit;
    private final World world;

    public BaseArena(@Nonnull String name, @Nonnull String alias, Location attackerSpawn, Location defenderSpawn, Location spectatorSpawn, Location exit) {
        super(name, alias);
        this.world = attackerSpawn.getWorld();
        this.attackerSpawn = attackerSpawn;
        this.defenderSpawn = defenderSpawn;
        this.spectatorSpawn = spectatorSpawn;
        this.exit = exit;
    }

    @Override
    public Location getAttackerSpawn() {
        return attackerSpawn;
    }

    @Override
    public Location getDefenderSpawn() {
        return defenderSpawn;
    }

    @Override
    public Location getSpectatorSpawn() {
        return spectatorSpawn;
    }

    @Override
    public Location getExit() {
        return exit;
    }

    @Override
    public World getWorld() {
        return world;
    }
}
