package me.arkenum.fightme.arena;

import me.arkenum.fightme.FightMe;
import me.ddevil.shiroi.misc.BukkitToggleable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by bruno on 09/11/2016.
 */
public class ArenaManager extends BukkitToggleable<FightMe> {
    private final Map<String, Arena> registeredArenas = new HashMap<>();

    public ArenaManager(FightMe plugin) {
        super(plugin);
    }

    public List<Arena> getRegisteredArenas() {
        return new ArrayList<>(registeredArenas.values());
    }

    public void registerArena(Arena arena) {
        String name = arena.getName();
        if (registeredArenas.containsKey(name)) {
            throw new IllegalArgumentException("This arena is already registered!");
        }
        registeredArenas.put(name, arena);
    }

    public Arena getArena(String name) {
        return registeredArenas.get(name);
    }
}

