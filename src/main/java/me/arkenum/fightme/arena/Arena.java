package me.arkenum.fightme.arena;

import me.arkenum.fightme.duel.handler.DuelHandler;
import me.ddevil.shiroi.misc.Nameable;
import me.ddevil.shiroi.misc.Serializable;
import org.bukkit.Location;
import org.bukkit.World;

public interface Arena extends Nameable, Serializable {
    Location getAttackerSpawn();

    Location getDefenderSpawn();

    Location getSpectatorSpawn();

    Location getExit();

    World getWorld();

    DuelHandler getDuelHandler();
}
