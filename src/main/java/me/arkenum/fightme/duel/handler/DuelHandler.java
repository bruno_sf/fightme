package me.arkenum.fightme.duel.handler;

import me.arkenum.fightme.duel.Duel;

/**
 * Created by bruno on 09/11/2016.
 */
public interface DuelHandler {

    Duel getCurrentDuel();

    boolean hasDuel();
}
