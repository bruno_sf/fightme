package me.arkenum.fightme.duel;

public enum DuelStatus {
    IN_QUEUE,
    BETTING,
    RUNNING,
    FINISHING,
    FINISHED;
}
