package me.arkenum.fightme.duel;

import me.arkenum.fightme.grouping.Squad;

public interface TeamDuel extends Duel {
    Squad getTeam(DuelPosition position);
}
