package me.arkenum.fightme.duel;

import me.arkenum.fightme.duel.bet.Bet;

import java.util.List;

public interface Duel {

    List<Bet> getAllBets();

    boolean acceptsMoneyBets();

    boolean acceptsItemBets();

    DuelStatus getDuelStatus();
}
