package me.arkenum.fightme.duel;

public enum DuelPosition {
    ATTACKER,
    DEFENDER;
}
