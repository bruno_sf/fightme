package me.arkenum.fightme.duel.bet;

import java.util.UUID;

/**
 * Created by bruno on 09/11/2016.
 */
public interface Bet {
    UUID getBeter();

    UUID getBetOn();
}
