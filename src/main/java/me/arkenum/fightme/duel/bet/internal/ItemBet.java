package me.arkenum.fightme.duel.bet.internal;

import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ItemBet extends BasicBet {
    private final List<ItemStack> items;

    public ItemBet(UUID beter, UUID betOn, List<ItemStack> items) {
        super(beter, betOn);
        this.items = items;
    }

    public List<ItemStack> getItems() {
        return new ArrayList<>(items);
    }
}
