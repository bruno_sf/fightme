package me.arkenum.fightme.duel.bet.internal;

import me.arkenum.fightme.duel.bet.Bet;

import java.util.UUID;

public abstract class BasicBet implements Bet {
    protected final UUID beter;
    protected final UUID betOn;

    public BasicBet(UUID beter, UUID betOn) {
        this.beter = beter;
        this.betOn = betOn;
    }

    @Override
    public UUID getBeter() {
        return beter;
    }

    @Override
    public UUID getBetOn() {
        return betOn;
    }
}
