package me.arkenum.fightme.duel.bet.internal;

import java.util.UUID;

/**
 * Created by bruno on 09/11/2016.
 */
public class MoneyBet extends BasicBet {
    private double amount;

    public MoneyBet(UUID beter, UUID betOn, double amount) {
        super(beter, betOn);
        this.amount = amount;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
