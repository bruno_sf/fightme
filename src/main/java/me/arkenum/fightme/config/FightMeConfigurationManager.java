package me.arkenum.fightme.config;

import me.arkenum.fightme.FightMe;
import me.ddevil.shiroi.config.ConstantsConfigurationManager;
import me.ddevil.shiroi.misc.PluginColorDesign;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.yaml.snakeyaml.Yaml;

import java.io.File;

public class FightMeConfigurationManager extends ConstantsConfigurationManager<FightMeConfigurationValue<?>> {
    public static final String MESSAGES_CONFIG = "messages";
    private static final String CONFIG_EXTENSION = ".yml";
    private final File arenasFolder;
    private final File messagesConfigFile;
    private final YamlConfiguration messagesConfig;

    public FightMeConfigurationManager(FightMe plugin) {
        super(plugin, new String[]{
                "config",
                "messages"
        });
        plugin.debug("Loading config...");
        this.messagesConfigFile = new File(plugin.getDataFolder(), MESSAGES_CONFIG+CONFIG_EXTENSION);
        this.messagesConfig = YamlConfiguration.loadConfiguration(messagesConfigFile);
        this.arenasFolder = new File(plugin.getDataFolder(), "arenas");
        if (!arenasFolder.exists()) {
            arenasFolder.mkdir();
        }
    }

    @Override
    protected FileConfiguration getFileConfiguration0(String s) {
        switch (s) {
            case MESSAGES_CONFIG:
                return messagesConfig;
            default:
                return defaultConfig;
        }
    }

    @Override
    public PluginColorDesign loadColorDesign() {
        FileConfiguration msgConfig = getFileConfiguration(MESSAGES_CONFIG);
        ConfigurationSection value = getValue(FightMeConfigurationValue.COLOR_DESIGN, msgConfig);
        return new PluginColorDesign(value);
    }
}
