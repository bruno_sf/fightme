package me.arkenum.fightme.config;

import me.ddevil.shiroi.config.BaseConfigurationValue;
import org.bukkit.configuration.ConfigurationSection;

public class FightMeConfigurationValue<T> extends BaseConfigurationValue<T> {
    public static final FightMeConfigurationValue<String> PLUGIN_PREFIX = new FightMeConfigurationValue<>(String.class, "general.pluginPrefix");
    public static final FightMeConfigurationValue<String> MESSAGE_SEPARATOR = new FightMeConfigurationValue<>(String.class, "general.messageSeparator");
    public static final FightMeConfigurationValue<String> HEADER = new FightMeConfigurationValue<>(String.class, "general.header");
    public static final FightMeConfigurationValue<Boolean> ARENA_ENABLED = new FightMeConfigurationValue<>(Boolean.class, "enabled");
    public static final FightMeConfigurationValue<String> ARENA_NAME = new FightMeConfigurationValue<>(String.class, "name");
    public static final FightMeConfigurationValue<String> ARENA_ALIAS = new FightMeConfigurationValue<>(String.class, "alias");
    public static final FightMeConfigurationValue<String> ARENA_WORLD = new FightMeConfigurationValue<>(String.class, "world");
    public static final FightMeConfigurationValue<ConfigurationSection> ARENA_ATTACKER_SPAWN = new FightMeConfigurationValue<>(ConfigurationSection.class, "attackerspawn");
    public static final FightMeConfigurationValue<ConfigurationSection> ARENA_DEFENDER_SPAWN = new FightMeConfigurationValue<>(ConfigurationSection.class, "defenderspawn");
    public static final FightMeConfigurationValue<ConfigurationSection> ARENA_SPECTATOR_SPAWN = new FightMeConfigurationValue<>(ConfigurationSection.class, "spectatorspawn");
    public static final FightMeConfigurationValue<ConfigurationSection> ARENA_EXIT = new FightMeConfigurationValue<>(ConfigurationSection.class, "exit");
    public static final FightMeConfigurationValue<String> COLOR_CHAR = new FightMeConfigurationValue<>(String.class, "colors.colorChar");
    public static final FightMeConfigurationValue<ConfigurationSection> COLOR_DESIGN = new FightMeConfigurationValue<>(ConfigurationSection.class, "colors.colorDesign");

    public FightMeConfigurationValue(Class<T> resultClass, String path) {
        super(resultClass, path);
    }
}
